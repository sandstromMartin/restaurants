const express = require('express');
const app = express();
const {PORT = 8080} = process.env;
const path = require('path');


//css and js files
app.use('/assets', express.static(path.join(__dirname,'public', 'static') ));

//route for my website, default
app.get('/', (req, res) => {
    return res.sendFile( path.join (__dirname, 'public', 'index.html'));
})
app.get('/data', (req, res) => {
    return res.sendFile( path.join (__dirname,  'public','data.json'));
})

app.get('/restaurant', (req, res) => {
    return res.sendFile( path.join (__dirname,  'public', 'restaurant.html'));
})


app.listen(PORT, ()=> console.log ('Server started on port ${PORT}...'));
